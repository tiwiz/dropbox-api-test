package aw.it.dptest;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.dropbox.sync.android.DbxAccount;
import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxDatastore;
import com.dropbox.sync.android.DbxDatastoreManager;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFields;
import com.dropbox.sync.android.DbxRecord;
import com.dropbox.sync.android.DbxTable;

public class DropboxActivity extends Activity implements View.OnClickListener{

    private DbxAccountManager dbxAccountManager;
    private DbxAccount dbxAccount;
    private DbxDatastore dbxDatastore = null;
    private DbxTable dbxTable = null;

    private Button btnSave;
    private Button btnLoad;
    private Button btnDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        initUI();
        //initDropbox();

        //inizializziamo l'account di Dropbox
        dbxAccountManager = DbxAccountManager.getInstance(getApplicationContext(),
                DropboxData.APP_KEY, //forniamo le nostre credenziali
                DropboxData.APP_SECRET);

        if(dbxAccountManager.hasLinkedAccount()){
            //prendiamo l'account se già collegato
            dbxAccount = dbxAccountManager.getLinkedAccount();
            //apriamo il datastore e il link alla tabella
            try {
                dbxDatastore = DbxDatastore.openDefault(dbxAccount);
                dbxTable = dbxDatastore.getTable("Test_AW.it");
                Log.d("DP-Test", "Tabella creata");
            } catch (DbxException e) {
                showError(e);
            }

        }else hideUI();
    }

    private void initUI(){

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        btnLoad = (Button) findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener(this);

        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(this);

    }

    private void hideUI(){

        btnSave.setVisibility(View.GONE);
        btnLoad.setVisibility(View.GONE);
        btnDelete.setVisibility(View.GONE);

    }

    private void showUI(){

        btnSave.setVisibility(View.VISIBLE);
        btnLoad.setVisibility(View.VISIBLE);
        btnDelete.setVisibility(View.VISIBLE);

    }

    private void showError(Exception e){
        String errorText = getString(R.string.generic_error_header) + e.getMessage();
        Toast.makeText(this,errorText,Toast.LENGTH_SHORT).show();
    }

    private void initDropbox(){

        //inizializziamo l'account di Dropbox
        dbxAccountManager = DbxAccountManager.getInstance(getApplicationContext(),
                DropboxData.APP_KEY, //forniamo le nostre credenziali
                DropboxData.APP_SECRET);

        if(dbxAccountManager.hasLinkedAccount()){
            //prendiamo l'account se già collegato
            dbxAccount = dbxAccountManager.getLinkedAccount();
            //apriamo il datastore e il link alla tabella
            try {
                dbxDatastore = DbxDatastore.openDefault(dbxAccount);
                dbxTable = dbxDatastore.getTable("Test_AW.it");
                Log.d("DP-Test", "Tabella creata");
            } catch (DbxException e) {
                showError(e);
            }

        }else hideUI();
    }

    private void SalvaRecordDropbox(){

        //salviamo il record di dropbox
        if(dbxAccountManager.hasLinkedAccount()){
            dbxTable.insert().set(DropboxData.DB_FIELD,DropboxData.DB_CONTENT);
            try {
                dbxDatastore.sync();
            } catch (DbxException e) {
                showError(e);
            }
        }
    }

    private DbxRecord RecuperaRecordDropbox(){

        DbxRecord dbxRecord = null;

        if(dbxAccountManager.hasLinkedAccount()){
            //creiamo la query di ricerca
            DbxFields dbxFields = new DbxFields().set(DropboxData.DB_FIELD,DropboxData.DB_CONTENT);
            try {
                //creiamo la query ed otteniamo il primo risultato
                DbxTable.QueryResult queryResults = dbxTable.query(dbxFields);
                dbxRecord = queryResults.iterator().next();
            } catch (DbxException e) {
                showError(e);
            }
        }

        return dbxRecord;
    }

    private void MostraRecordDropbox(){

        //recuperiamo il record
        DbxRecord dbxRecord = RecuperaRecordDropbox();

        String message = "";

        if(dbxRecord != null)
            message = getString(R.string.msg_load_ok) + dbxRecord.getString(DropboxData.DB_FIELD);
        else
            message = getString(R.string.msg_load_failure);

        Toast.makeText(this, message,Toast.LENGTH_SHORT).show();
    }

    private void EliminaRecordDropbox(){

        DbxRecord dbxRecord = RecuperaRecordDropbox();

        String message = "";

        if(dbxRecord != null){
            //cancello il record
            dbxRecord.deleteRecord();
            message = getString(R.string.msg_record_deleted);
            try {
                dbxDatastore.sync();
            } catch (DbxException e) {
                showError(e);
            }
        }else
            message = getString(R.string.msg_load_failure);

        Toast.makeText(this, message,Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dropbox, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.action_link_dropbox:
                //gestiamo la connessione all'account Dropbox
                dbxAccountManager.startLink(DropboxActivity.this,DropboxData.REQUEST_CODE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //gestiamo il ritorno del login, filtrando solamente le richieste che hanno il codice che ci interessa
        if (requestCode == DropboxData.REQUEST_CODE) {
            //se il login è stato effettuato, siamo a posto
            if (resultCode == Activity.RESULT_OK) {
                dbxAccount = dbxAccountManager.getLinkedAccount();
                showUI();
            } else {
                //altrimenti, avvisiamo l'utente
                Toast.makeText(this,getString(R.string.msg_link_failed),Toast.LENGTH_SHORT).show();
                hideUI();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View view) {

        int clickedViewId = view.getId();

        switch(clickedViewId){
            case R.id.btnSave:
                SalvaRecordDropbox();
                break;
            case R.id.btnLoad:
                MostraRecordDropbox();
                break;
            case R.id.btnDelete:
                EliminaRecordDropbox();
                break;
        }
    }
}
